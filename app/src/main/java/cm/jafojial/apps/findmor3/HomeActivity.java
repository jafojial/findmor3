package cm.jafojial.apps.findmor3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.OnFragmentInteractionListener,
        SearchFragment.OnFragmentInteractionListener {

    private static final int CAMERA_REQUEST_CODE = 100;
    private static final int GALLERY_REQUEST_CODE = 101;

    FloatingActionButton fabSearch;
    FloatingActionButton fabCamera;
    FloatingActionButton fabGallery;
    //CoordinatorLayout rootLayout;

    //Save the FAB's active status
    //false -> fab = close
    //true -> fab = open
    private boolean fabStatus = false;

    //Animations
    Animation showFabCamera;
    Animation hideFabCamera;
    Animation showFabGallery;
    Animation hideFabGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Floating Action Buttons
        fabSearch = (FloatingActionButton) findViewById(R.id.fab_search);
        fabCamera = (FloatingActionButton) findViewById(R.id.fab_camera);
        fabGallery = (FloatingActionButton) findViewById(R.id.fab_gallery);

        //Animations
        showFabCamera = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_camera_show);
        hideFabCamera = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_camera_hide);
        showFabGallery = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_gallery_show);
        hideFabGallery = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_gallery_hide);
/*
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/
        fabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (fabStatus == false) {
                    //Display FAB menu
                    expandFAB();
                    fabSearch.setImageResource(R.drawable.ic_close_white);
                    fabStatus = true;
                } else {
                    //Close FAB menu
                    hideFAB();
                    fabStatus = false;
                    fabSearch.setImageResource(R.drawable.ic_search_white);
                }
            }
        });

        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFAB();
                fabStatus = false;
                fabSearch.setImageResource(R.drawable.ic_search_white);
                Toast.makeText(getApplication(), "Floating Action Button Camera", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, CAMERA_REQUEST_CODE);
            }
        });

        fabGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFAB();
                fabStatus = false;
                fabSearch.setImageResource(R.drawable.ic_search_white);
                Toast.makeText(getApplication(), "Floating Action Button Gallery", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),GALLERY_REQUEST_CODE);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {

            HomeFragment homeFragment = new HomeFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, homeFragment).commit();

            homeFragment.getView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (fabStatus) {
                        hideFAB();
                        fabStatus = false;
                    }
                    return false;
                }
            });

            //drawer.openDrawer(GravityCompat.START);
        }else {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            if(fragments != null){
                for(Fragment fragment : fragments){
                    if(fragment != null && fragment.isVisible()){
                        if(!(fragment.getClass().getSimpleName().equalsIgnoreCase("HomeFragment"))){
                            fabSearch.setVisibility(View.INVISIBLE);
                        }

                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) intent.getExtras().get("data");
            //imageView.setImageBitmap(photo);
            SearchFragment sf = SearchFragment.newInstance(photo,"");
            //sf.imageToSearch.setImageBitmap(photo);
            replaceFragment(sf);
        }
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (intent!=null){
                try{
                    Bitmap photo = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), intent.getData());
                    SearchFragment sf = SearchFragment.newInstance(photo,"");
                    //sf.imageToSearch.setImageBitmap(photo);
                    replaceFragment(sf);
                } catch (IOException e){
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onActivityResult(){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the action
            replaceFragment(new HomeFragment());
        } else if (id == R.id.nav_cart) {
            replaceFragment(new HomeFragment());
        } else if (id == R.id.nav_feedback) {
            replaceFragment(new HomeFragment());
        } else if (id == R.id.nav_settings) {
            replaceFragment(new HomeFragment());
        } else if (id == R.id.nav_about) {
            replaceFragment(new HomeFragment());
        } else if (id == R.id.nav_share) {
            replaceFragment(new HomeFragment());
        } else if (id == R.id.nav_send) {
            replaceFragment(new HomeFragment());
        } else if (id == R.id.nav_logout) {
            replaceFragment(new HomeFragment());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
        if(!(fragment.getClass().getSimpleName().equalsIgnoreCase("HomeFragment"))){
            fabSearch.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void expandFAB() {

        //Floating Action Button Camera
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fabCamera.getLayoutParams();
        layoutParams.rightMargin += (int) (fabCamera.getWidth() * 1.7);
        layoutParams.bottomMargin += (int) (fabCamera.getHeight() * 0.25);
        fabCamera.setLayoutParams(layoutParams);
        fabCamera.startAnimation(showFabCamera);
        fabCamera.setClickable(true);

        //Floating Action Button Gallery
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fabGallery.getLayoutParams();
        layoutParams2.rightMargin += (int) (fabGallery.getWidth() * 3.0);
        layoutParams2.bottomMargin += (int) (fabGallery.getHeight() * 0.25);
        fabGallery.setLayoutParams(layoutParams2);
        fabGallery.startAnimation(showFabGallery);
        fabGallery.setClickable(true);

    }

    private void hideFAB() {

        //Floating Action Button Camera
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fabCamera.getLayoutParams();
        layoutParams.rightMargin -= (int) (fabCamera.getWidth() * 1.7);
        layoutParams.bottomMargin -= (int) (fabCamera.getHeight() * 0.25);
        fabCamera.setLayoutParams(layoutParams);
        fabCamera.startAnimation(hideFabCamera);
        fabCamera.setClickable(false);

        //Floating Action Button Gallery
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fabGallery.getLayoutParams();
        layoutParams2.rightMargin -= (int) (fabGallery.getWidth() * 3.0);
        layoutParams2.bottomMargin -= (int) (fabGallery.getHeight() * 0.25);
        fabGallery.setLayoutParams(layoutParams2);
        fabGallery.startAnimation(hideFabGallery);
        fabGallery.setClickable(false);

    }
}
